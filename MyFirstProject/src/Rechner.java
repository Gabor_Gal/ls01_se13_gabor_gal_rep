import java.util.Scanner;

public class Rechner {

	public static double plus(double zahl1,double zahl2){
	return zahl1 + zahl2;}
	
	public static double minus(double zahl1,double zahl2){
	return zahl1 - zahl2;}
	
	public static double mal(double zahl1,double zahl2){
	return zahl1 * zahl2;}
	
	public static double durch(double zahl1,double zahl2){
	return zahl1 / zahl2;}	
	
	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);
		
		double eingabe1;
		double eingabe2;
		double ergebnis = 0;
		char rechenoperation;
		
		System.out.println("Geben sie die erste Zahl ein: ");
		eingabe1 = tastatur.nextDouble();
		
		System.out.println("Welche Rechenoperation wollen sie tun?");
		rechenoperation = tastatur.next().charAt(0);
		
		System.out.println("Geben sie die zweite Zahl ein: ");
		eingabe2 = tastatur.nextDouble();
		
		if(rechenoperation == '+'){
			ergebnis = plus(eingabe1, eingabe2);
		}
		
		
		if(rechenoperation == '-'){
			ergebnis = minus(eingabe1, eingabe2);
		}
		
		
		if(rechenoperation == '*'){
			ergebnis = mal(eingabe1, eingabe2);
		}
		
		
		if(rechenoperation == '/'){
			ergebnis = durch(eingabe1, eingabe2);
		}
		
		else {System.out.println("Falsche Eingabe");}
		
		System.out.println("Das Ergebnis lautet:");
		System.out.printf("%.2f", ergebnis);
		tastatur.close();
	}	
}
