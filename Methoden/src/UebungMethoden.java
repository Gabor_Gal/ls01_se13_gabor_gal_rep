import java.util.Scanner;

public class UebungMethoden {

	public static void main(String[] args) {
		
		String artikel = bestellEingabe();
		int anzahl = anzahlEingabe();
		double preis = nettoEingabe();
		double mwst = mwstEingabe();
		
		//Verarbeiten
		double nettogesamtpreis = nettoVerarbeitung(anzahl, preis);
		double bruttogesamtpreis = bruttoVerarbeitung(nettogesamtpreis, mwst);
		
		// Ausgeben
		ausgabe(artikel, anzahl, preis, mwst, nettogesamtpreis, bruttogesamtpreis);

	}
//###########################################################################################################################################	
//Methoden
//###########################################################################################################################################
	
	static Scanner tastatur = new Scanner(System.in);
	
	public static String bestellEingabe(){
	System.out.println("Was m�chten sie bestellen?");
	return tastatur.nextLine();}	
	
	public static int anzahlEingabe(){
	System.out.println("Geben sie die Anzahl an:");
	return tastatur.nextInt();}	
	
	public static double nettoEingabe(){
	System.out.println("Geben sie den Nettopreis ein:");
	return tastatur.nextDouble();}	
	
	public static double mwstEingabe(){
	System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
	return tastatur.nextDouble();}	
	
//###########################################################################################################################################	
	
    public static double nettoVerarbeitung(double anzahl, double preis){
    return (anzahl * preis);}	
    		
    public static double bruttoVerarbeitung(double nettogesamtpreis, double mwst) {	
	return (nettogesamtpreis * (1 + mwst / 100));}  	
    	
//###########################################################################################################################################	
	
    public static void ausgabe(String artikel, int anzahl, double preis, double mwst, double nettogesamtpreis, double bruttogesamtpreis){	
	System.out.println("\tRechnung");
	System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
	System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");}
    	
    
}