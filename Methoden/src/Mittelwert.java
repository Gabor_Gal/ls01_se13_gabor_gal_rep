//import java.lang.reflect.Array;
import java.util.Scanner;

public class Mittelwert {
	
	public static double eingabe1() {
		   double eingabe1;
		   Scanner tastatur = new Scanner(System.in);
		   System.out.print("Geben sie die erste Zahl an: ");
		   eingabe1 = tastatur.nextDouble();
	return eingabe1;}
	
	public static double eingabe2() {
		   double eingabe2;
		   Scanner tastatur = new Scanner(System.in);
		   System.out.print("Geben sie die erste Zahl an: ");
		   eingabe2 = tastatur.nextDouble();
		   tastatur.close();
	return eingabe2;}
	
	public static double verarbeitung(double zahl1, double zahl2) {
	return (zahl1 + zahl2) / 2.0;}
	
	public static double ausgabe(double zahl1, double zahl2, double ergebnis) {
		System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", zahl1, zahl2, ergebnis);
	return 0;}
	
//#################################################################
// MAIN METHODE
//#################################################################
	
   public static void main(String[] args) {

      // (E) "Eingabe"
      double x;
      double y;
      double m;
      
      x = eingabe1();
      y = eingabe2(); 
      
      // (V) Verarbeitung
      m = verarbeitung(x,y);
      
      // (A) Ausgabe
      ausgabe(x,y,m);
   }
   
   
/*     //R�ckgabe von 2 Werten mittels eines Arrays
 public static double eingabe(){
	   double eingabe1, eingabe2;
	   Scanner tastatur = new Scanner(System.in);
	   System.out.print("Geben sie die erste Zahl an: ");
	   eingabe1 = tastatur.nextDouble();
	   System.out.print("Geben sie die zweite Zahl an: ");
	   eingabe2 = tastatur.nextDouble();
	   double[] arr = new double[2];
	   arr[0] = eingabe1;
	   arr[1] = eingabe2;
	   return arr;
   }
*/

   }
   
   

