
public class Temperatur {

	public static void main(String[] args) {

		//Aufgabe 3 Temperaturtabelle
		
		int fahrenheit1 = -20;			double celsius1 = -28.8889;
		int fahrenheit2 = -10;			double celsius2 = -23.3333;
		int fahrenheit3 = +0; 			double celsius3 = -17.7778;
		int fahrenheit4 = +20;			double celsius4 = -6.6667;
		int fahrenheit5 = +30;			double celsius5 = -1.1111;
		
		// Convertierung von Integer zu String um das + vor den Zahlen hinzu zu f�gen
		String fahrenheit3a = String.valueOf(fahrenheit3);
		fahrenheit3a = "+" + fahrenheit3a;
		String fahrenheit4a = String.valueOf(fahrenheit4);
		fahrenheit4a = "+" + fahrenheit4a;
		String fahrenheit5a = String.valueOf(fahrenheit5);
		fahrenheit5a = "+" + fahrenheit5a;
		
		// Ausgabe der Tabelle
		System.out.print("Fahrenheit  |   Celsius\n-----------------------\n");
		System.out.printf( "%-12d|%10.2f\n", fahrenheit1, celsius1);
		System.out.printf( "%-12d|%10.2f\n", fahrenheit2, celsius2);
		System.out.printf( "%-12s|%10.2f\n", fahrenheit3a, celsius3);
		System.out.printf( "%-12s|%10.2f\n", fahrenheit4a, celsius4);
		System.out.printf( "%-12s|%10.2f\n", fahrenheit5a, celsius5);
				
	}

}
