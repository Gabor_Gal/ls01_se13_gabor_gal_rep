/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 08.01.2018
  * @author 
  */

public class LogischeOperatoren {
  
  public static void main(String[] args) {
    /* 1. Deklarieren Sie zwei Warheitswerte a und b.*/
	  
	  boolean wert1, wert2;
    
    /* 2. Initialisieren Sie einen Wert mit true, den anderen mit false */
	  
	  wert1 = true;
	  wert2 = false;
    
    /* 3. Geben Sie beide Werte aus */
	  
	  System.out.println(wert1+" "+wert2);
    
    /* 4. Deklarieren Sie einen Wahrheitswert undGatter */
	  
	  boolean undGatter;
	  
	  boolean a = wert1;
	  boolean b = wert2;
	  
	  
    /* 5. Weisen Sie der Variable undGatter den Wert "a AND b" zu 
          und geben Sie das Ergebnis aus. */
	  
	  undGatter = a & b;
	  System.out.println(undGatter);
          
    /* 6. Deklarieren Sie au�erdem den Wahrheitswert c und initialisieren ihn
          direkt mit dem Wert true */
	  
	  boolean c = true;
          
    /* 7. Verkn�pfen Sie alle drei Wahrheitswerte a, b und c und geben Sie
          jeweils das Ergebnis aus */
       // a)  a AND b AND c
	  System.out.println(a && b && c);
       // b)  a OR  b OR  c
	  System.out.println(a && b || c);
       // c)  a AND b OR  c
	  System.out.println(a || b && c);
       // d)  a OR  b AND c
	  System.out.println(a || b && c);
       // e)  a XOR b AND c
	  System.out.println(a ^ b && c);
       // f) (a XOR b) OR c
        } // end of main

} // end of class LogischeOperatoren
