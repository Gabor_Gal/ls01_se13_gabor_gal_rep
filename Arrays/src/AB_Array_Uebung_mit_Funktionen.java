import java.util.Arrays;
import java.util.Scanner;

public class AB_Array_Uebung_mit_Funktionen {

//##################################################################
//Methoden
//##################################################################	
	
	static Scanner tastatur = new Scanner(System.in);
	public static int eingabe() {
		return tastatur.nextInt();}
	//Aufgabe 1
	public static String convertArrayToString(int[] zahlen) {
		return Arrays.toString(zahlen);
	}
	
	//Aufgabe 2
	public static String arrayUmdrehen(int array[]) {
		int hilfsvariable;
		System.out.println("Gegebenes Array: " + Arrays.toString(array));
		
		hilfsvariable = array[0];
		array[0] = array[6];					
		array[6] = hilfsvariable;
		
		hilfsvariable = array[1];
		array[1] = array[5];							
		array[5] = hilfsvariable;
		
		hilfsvariable = array[2];
		array[2] = array[4];							
		array[4] = hilfsvariable;
		
		return Arrays.toString(array);
	}
	
	//Aufgabe 3
	public static String arrayUmdrehenMit2Arrays(int array[]) {
		System.out.println("Gegebenes Array: " + Arrays.toString(array));
		int[] array2 = new int[7];
		int zaehler = 0;
		int zaehler2 = 6;
		while (zaehler < 7) {
			array2[zaehler2] = array[zaehler];
			zaehler++;
			zaehler2--;
		}
		return Arrays.toString(array2);
	}
	
	//Aufgabe 4
	public static String temperaturArray(double arrayZeile1[], int arrayZeile) {
		
		double arrayZeile2[] = {(5/9)*(arrayZeile1[1]-32), (5/9)*(arrayZeile1[2]-32)};
		double[][] zweidimArray = {arrayZeile1, arrayZeile2};
		
	return Arrays.toString(zweidimArray[arrayZeile]);}
	
	
	
	
//##################################################################
//Main Methode
//##################################################################	
		
	public static void main(String[] args) {
	
	System.out.println("Welche Aufgabe wollen sie haben? (1-6)");
	int aufgabe = eingabe();	
		
//##################################################################
//Aufgabe 1
//##################################################################
			
	if (aufgabe == 1) {
		
		int[] array = new int[] {1, 2, 4, 8, 16, 32, 64};
		System.out.println(convertArrayToString(array));
		// Methode an dieser Stelle ist nicht Sinnvoll, man k�nnte direkt: Arrays.toString(array) verwenden
		
	}
		
//##################################################################
//Aufgabe 2
//##################################################################
		
	if (aufgabe == 2) {
		
		int[] array = new int[] {1, 2, 4, 8, 16, 32, 64};
		System.out.print(arrayUmdrehen(array));
		
	}	
		
//##################################################################
//Aufgabe 3
//##################################################################
			
	if (aufgabe == 3) {
		
		int[] array = new int[] {1, 2, 4, 8, 16, 32, 64};
		System.out.print(arrayUmdrehenMit2Arrays(array));
	}	

//##################################################################
//Aufgabe 4
//##################################################################
				
	if (aufgabe == 4) {
		
		double[] array = new double[] {0.0, 10.0};
		System.out.println(temperaturArray(array, 0)+" Fahrenheit");
		System.out.println(temperaturArray(array, 1)+" Grad Celsius");
		
	}		
		
//##################################################################
//Aufgabe 5
//##################################################################
					
	if (aufgabe == 5) {
		
		System.out.println((5/9)*(0.0-32));
		System.out.println((10.0-32)*(5/9));
		
		
	}		
		
//##################################################################
//Aufgabe 6
//##################################################################
						
	if (aufgabe == 6) {
							
	}	
		
		
		
		//TEST
		
		
	}

}
