/**
  *
  * if-Abfrage-11 Auswahl_03
  *
  * @version 1.0 from 22.03.2022
  * @author 
  */

public class vorbereitungKlausur6 {

  public static void main(String[] args) {
     int x = 7, y = 3, z = 9;
    if (x<y || (z-y) <= x){
      x = z-3;
      y = x++;
     // System.out.println("y: " + y);  // 6
     // System.out.println("x: " + x);  // 7
      ++z;
     //  System.out.println("z: " + z);  // 10
      }
    else{
      x = 2+y;
      y = z-x;
      z--;
      }
     x++;
    //System.out.println("x: " + x); // 8
    System.out.println(x+y+z);
  }
}
