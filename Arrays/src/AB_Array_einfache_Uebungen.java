import java.util.Arrays;
import java.util.Scanner;

public class AB_Array_einfache_Uebungen {

	static Scanner tastatur = new Scanner(System.in);
	
	public static int eingabe() {
		return tastatur.nextInt();}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Welche Aufgabe wollen sie haben? (1-4)");
		int aufgabe = eingabe();
		
//##################################################################
//Aufgabe 1
//##################################################################

		
	if (aufgabe == 1) {		
		int[] array = new int[10];
		
		int zaehler = 0;
		while(zaehler <= 9) {
			array[zaehler] = zaehler;
			zaehler++;
		}
		
		zaehler = 0;
		while(zaehler <= 9) {
			System.out.print(array[zaehler]+" ");
			zaehler++;
		}
	}
		
//##################################################################
//Aufgabe 2
//##################################################################		
		
	if (aufgabe == 2) {	
		int[] array = new int[10];
		
		int zaehler = 1;
		int zaehler2 = 0;
		while(zaehler <= 19) {
			array[zaehler2] = zaehler;
			zaehler = zaehler + 2;
			zaehler2++;
		}
		
		zaehler = 1;
		zaehler2 = 0;
		while(zaehler <= 19) {
			System.out.print(array[zaehler2]+" ");
			zaehler = zaehler + 2;
			zaehler2++;
		}
	}
		
//##################################################################
//Aufgabe 3
//##################################################################		
			
	if (aufgabe == 3) {	
		
		int zaehler = 0;
		int zaehler2 = 1;
		String[] array = new String[5];
			
			while(zaehler < 5) {
				System.out.println("Geben sie ihre " + zaehler2 + ". Zahl ein:");
				array[zaehler] = tastatur.nextLine();
				zaehler++;
				zaehler2++;
			}
			
			zaehler = 4;
			while(zaehler >= 0) {
			System.out.println(array[zaehler]);
			zaehler--;
			}	
	}		
		
//##################################################################
//Aufgabe 4
//##################################################################		
				
	if (aufgabe == 4) {	
			
		int[] array = new int[] {3, 7, 12, 18, 37, 42};
		System.out.println(Arrays.toString(array)+"\n");
		
		int zaehler = 0;
		boolean zahl13vorhanden = false;
		while (zaehler < 5) {
		if (array[zaehler] == 12) {System.out.println("Die Zahl 12 ist in der Ziehung enthalten");}
		if (array[zaehler] == 13) {zahl13vorhanden = true;}
		zaehler++;}
		
		if (zahl13vorhanden == false) {System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten");}
	
	}
		
		
				
		
		
	}

}
