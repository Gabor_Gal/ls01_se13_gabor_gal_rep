import java.util.Scanner;

public class FahrkartenautomatMitMethoden {

//##################################################################
// METHODEN
//##################################################################
	static Scanner tastatur = new Scanner(System.in);
//##################################################################################################################################	
	
	public static double fahrkartenbestllungErfassen(){
		boolean korrekteAnzahl = false;
		double zuZahlenderBetrag = 0;
		while(korrekteAnzahl == false){
			System.out.print("Zu zahlender Betrag (EURO): ");
			zuZahlenderBetrag = tastatur.nextDouble();
			if (zuZahlenderBetrag < 0){
				System.out.println("Der Ticketpreis darf nicht unter 0� liegen");}
			else {korrekteAnzahl = true;}
		}
	return zuZahlenderBetrag;}

//##################################################################################################################################
	
	public static byte anzahlFahrkarten(){
		boolean korrekteAnzahl = false;
		byte anzahlFahrkarten = 0;
		while(korrekteAnzahl == false){
			System.out.println("Wie viele Tickets sollen es sein?");
			anzahlFahrkarten = tastatur.nextByte();
			if(anzahlFahrkarten > 10 || anzahlFahrkarten <=0){
				System.out.println("Ung�ltige Eingabe, bitte beachten sie dass sie Maximal 10 Tickets kaufen k�nnen");}
			else {korrekteAnzahl = true;}
	    	}								// Wurde mit Herrn G�kcen abgesprochen das wie die Frage wiederholen und nicht mit 1 weiter machen
	return anzahlFahrkarten;}

//##################################################################################################################################
	
	public static double fahrkartenBezahlen(double zuZahlenderBetrag, byte kartenAnzahl){
	    zuZahlenderBetrag = zuZahlenderBetrag * kartenAnzahl;
	    double eingezahlterGesamtbetrag = 0.00;
        double eingeworfeneM�nze = 0.00;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
	    	   System.out.format("%s %.2f %s\n","Noch zu zahlen:", zuZahlenderBetrag - eingezahlterGesamtbetrag, "Euro");
	    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	    	   eingeworfeneM�nze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;}
	return (eingezahlterGesamtbetrag - zuZahlenderBetrag);}
	
//##################################################################################################################################
	
	public static void fahrkartenAusgeben(byte anzahlTickets){
		System.out.println("\nFahrschein wird ausgegeben (Je nach Menge der Tickets, dauert der Durckvorgang l�nger)");
		warten(250,anzahlTickets);
	       System.out.println("\n\n");}
	
//##################################################################################################################################
	
	public static void warten(int milisekunden,byte anzahlTickets) {
		System.out.println("Druck Status:");
		int i = 0; 
		while(i<(8+anzahlTickets)) {
			i++;
			System.out.print("=");
			if (milisekunden > 1){
		        try {
		            Thread.sleep(milisekunden);
		        } catch (InterruptedException ie) {}}}}       
	
//##################################################################################################################################
		
	public static void muenzenAusgeben(double rueckgabebetrag){
		if(rueckgabebetrag > 0.0){
			
	    	   System.out.printf("%s %.2f %s\n","Der R�ckgabebetrag in H�he von", rueckgabebetrag, "EURO");
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(rueckgabebetrag >= 2.0) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2 EURO");
		          rueckgabebetrag -= 2.0;
	           }
	           while(rueckgabebetrag >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 EURO");
		          rueckgabebetrag -= 1.0;
	           }
	           while(rueckgabebetrag >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
		          rueckgabebetrag -= 0.5;
	           }
	           while(rueckgabebetrag >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	 	          rueckgabebetrag -= 0.2;
	           }
	           while(rueckgabebetrag >= 0.1) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
		          rueckgabebetrag -= 0.1;
	           }
	           while(rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	 	          rueckgabebetrag -= 0.05;
	           }
	       }

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir w�nschen Ihnen eine gute Fahrt.");}
	
//#################################################################
// MAIN METHODE
//#################################################################	
	
	public static void main(String[] args) {
		
		byte anzahlTickets;
		double zuZahlenderBetrag;
		double rueckgeld;
		
		zuZahlenderBetrag = fahrkartenbestllungErfassen();
		anzahlTickets = anzahlFahrkarten();
		rueckgeld = fahrkartenBezahlen(zuZahlenderBetrag, anzahlTickets);
		fahrkartenAusgeben(anzahlTickets);
		muenzenAusgeben(rueckgeld);

	}

}

