import java.util.Scanner;

public class FahrkartenautomatInBesser {

//##################################################################
// METHODEN
//##################################################################

	static Scanner tastatur = new Scanner (System.in);
	static double zuZahlenderBetrag = 0;
	static double eingeworfeneMuenzen = 0;
	static double eingeworfeneMuenze = 0;
	static byte anzahlTickets = 0;
	static boolean korrekteAnzahl = false;
	static double rueckgeld = 0;
	static boolean fahrkartenautomatAN = true;
	static 	String[] bezeichnung = new String[10];
	static 	double[] preise = new double[10];
	static int ticketAuswahl = 0;
	
//##################################################################
	
	public static void wertzuweisung() {
		bezeichnung[0] = "Einzelfahrschein Berlin AB";
		bezeichnung[1] = "Einzelfahrschein Berlin BC";
		bezeichnung[2] = "Einzelfahrschein Berlin ABC";
		bezeichnung[3] = "Kurzstrecke";
		bezeichnung[4] = "Tageskarte Berlin AB";
		bezeichnung[5] = "Tageskarte Berlin BC";
		bezeichnung[6] = "Tageskarte Berlin ABC";
		bezeichnung[7] = "Kleingruppen-Tageskarte Berlin A";
		bezeichnung[8] = "Kleingruppen-Tageskarte Berlin AB";		
		bezeichnung[9] = "Kleingruppen-Tageskarte Berlin ABC";
		
		preise[0] = 2.90;
		preise[1] = 3.30;	
		preise[2] = 3.60;
		preise[3] = 1.90;
		preise[4] =	8.60;	
		preise[5] =	9.00;	
		preise[6] =	9.60;	
		preise[7] =	23.50;	
		preise[8] =	24.30;	
		preise[9] =	24.90;	
	}
	
//##################################################################	
	
	public static void fahrkartenbestellungErfassen() {
		while (ticketAuswahl <= 0) {
			System.out.println("9999 zum Bennden");
			System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:");
			for(int i = 0; i < preise.length; i++) {
				System.out.print(bezeichnung[i] +"["+ preise[i] +"0 EUR] ("+ (i+1) +")\n");}			
			ticketAuswahl = tastatur.nextInt();
			
			if (ticketAuswahl < 0 && ticketAuswahl > bezeichnung.length) {
				System.out.println("Das ausgew�hlte Ticket exsistiert nicht (1-10)");}
			if (ticketAuswahl == 9999) {System.exit(0);}}
		
		zuZahlenderBetrag = preise[ticketAuswahl-1];	
	}

//##################################################################
	
	public static void anzahlFahrkarten() {
		while(korrekteAnzahl == false) {
			System.out.println("Wie viele Tickets sollen es sein?");
			anzahlTickets = tastatur.nextByte();
			if(anzahlTickets > 10 || anzahlTickets <=0){
				System.out.println("Ung�ltige Eingabe! \nBitte beachten sie dass sie Maximal 10 Tickets kaufen k�nnen.");}
			else {korrekteAnzahl = true;}	// Wurde mit Herrn G�kcen abgesprochen das wie die Frage wiederholen und nicht mit 1 weiter machen
		}	
	}
	
//##################################################################	
	
	public static void fahrkartenBezahlen() {
		double eingezahlterGesamtbetrag = 0;
		zuZahlenderBetrag = zuZahlenderBetrag * anzahlTickets;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
	    	System.out.format("%s %.2f %s\n","Noch zu zahlen:", zuZahlenderBetrag - eingezahlterGesamtbetrag, "Euro");
	    	System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	    	eingeworfeneMuenze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMuenze;} 
	    rueckgeld = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	}
	
//##################################################################
	
	public static void fahrkartenAusgeben(){
		System.out.println("\nFahrschein wird ausgegeben (Je nach Menge der Tickets, dauert der Durckvorgang l�nger)");
		warten("Druckstatus:", 250);
	       System.out.println("\n\n");}
	
//##################################################################
	
	public static void warten(String Text, int milisekunden) {
		System.out.println(Text);
		int i = 0; 
		while(i<(8+anzahlTickets)) {
			i++;
			System.out.print("=");
			if (milisekunden > 1){
		        try {
		            Thread.sleep(milisekunden);
		        } catch (InterruptedException ie) {}}}} 
	
//##################################################################
	
	public static void rueckgeldAusgeben(){
		
		System.out.println("Sie haben "+ anzahlTickets +"x "+ bezeichnung[ticketAuswahl-1] +" bestellt");
		
		if(rueckgeld > 0.0){
				
	    	   System.out.printf("%s %.2f %s\n","Der R�ckgabebetrag in H�he von", rueckgeld, "EURO");
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(rueckgeld >= 2.0) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2 EURO");
	        	  rueckgeld -= 2.0;
	           }
	           while(rueckgeld >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 EURO");
	        	  rueckgeld -= 1.0;
	           }
	           while(rueckgeld >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
	        	  rueckgeld -= 0.5;
	           }
	           while(rueckgeld >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	        	  rueckgeld -= 0.2;
	           }
	           while(rueckgeld >= 0.1) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
	        	  rueckgeld -= 0.1;
	           }
	           while(rueckgeld >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	        	  rueckgeld -= 0.05;
	           }
	       }

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir w�nschen Ihnen eine gute Fahrt.");}

//##################################################################	
	
	public static void clear() { 
		System.out.println("");
		warten("Neustart",400);
		System.out.print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		zuZahlenderBetrag = 0;
		eingeworfeneMuenzen = 0;
		eingeworfeneMuenze = 0;
		anzahlTickets = 0;
		korrekteAnzahl = false;
		rueckgeld = 0;
		fahrkartenautomatAN = true;
		ticketAuswahl = 0;
	}  

//#################################################################
// MAIN METHODE
//#################################################################	
	
	public static void main(String[] args) {
		
		wertzuweisung();	
while (fahrkartenautomatAN == true) {
		fahrkartenbestellungErfassen();
		anzahlFahrkarten();
		fahrkartenBezahlen();
		fahrkartenAusgeben();
		rueckgeldAusgeben();
		clear();}
	}

}
